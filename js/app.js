var cartCounter = 0;

function openCart(){
    if (!$('.main-checkout, .main-checkout .checkout-inner').hasClass('active')){
        $('.main-checkout, .main-checkout .checkout-inner').addClass('active');
        $('#cart-icon').addClass('active');
        $('body').addClass('no-scroll');
    }else{
        $('.main-checkout, .main-checkout .checkout-inner').removeClass('active');
        $('#cart-icon').removeClass('active');
        $('body').removeClass('no-scroll');
    }
}


function openSearch(){
    $('#search-icon').addClass('active');
    $('.menu-links').addClass('hidden');
    $('.search-wrapper').addClass('active');
}

function closeSearch(){
    $('#search-icon').removeClass('active');
    $('.menu-links').removeClass('hidden');
    $('.search-wrapper').removeClass('active');
    $('.search-box input').val('');
}

function goto(name){
    $('.menu-btn').removeClass('active');
    $('.card').removeClass('active');
    $('body').removeClass('no-scroll');
    $('#cart-icon').removeClass('active');
    $('#my-checkout.main-checkout, #my-checkout.checkout-inner').removeClass('active');

    if (name === 'pos'){
        $('#pos-link').addClass('active');
        $('#my-products.card').addClass('active');
    }

    if (name === 'wallet'){
        $('#wallet-link').addClass('active');
        $('#my-wallet.card').addClass('active');
    }

    if (name === 'establishment'){
        $('#establishment-link').addClass('active');
        $('#my-establishment.card').addClass('active');
    }
}

$('.product-item').click(() => {
    cartCounter += 1;
    $('.cart-counter p').text(cartCounter);
});

function addVirtualMoney(){
    $('#virtual-money').attr('style', 'display: flex');
    $('#checkout-total').attr('style', 'display: flex');
    $('#virtual-text').attr('style', 'opacity: 0');
}